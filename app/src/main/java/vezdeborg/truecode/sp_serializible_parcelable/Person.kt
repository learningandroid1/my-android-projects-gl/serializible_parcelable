package vezdeborg.truecode.sp_serializible_parcelable

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Person(
    val name: String = "Alexey",
    val age: Int = 27,
    val workPlace: String = "ASTON"
): Parcelable {
}