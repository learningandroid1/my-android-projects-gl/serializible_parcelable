package vezdeborg.truecode.sp_serializible_parcelable

import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import vezdeborg.truecode.sp_serializible_parcelable.databinding.ActivityMainBinding

private const val PREFERENCE_NAME = "prefs_name"
private const val KEY_STRING_NAME = "KEY_STRING"

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var prefs: SharedPreferences

    private val person = Person()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        prefs = getSharedPreferences(PREFERENCE_NAME, MODE_PRIVATE)
        val editor = prefs.edit()
        editor.putString(KEY_STRING_NAME, "Я хуею от Скиллбокса").apply()
        //editor.remove(KEY_STRING_NAME)
        //prefs.clear()

        //prefs.contains(KEY_STRING_NAME)
        binding.buttonForLoading.setOnClickListener {
            val ourString = prefs.getString(KEY_STRING_NAME, "default")
            binding.textForLoading.text = ourString
        }

        val fragmentArgs = Bundle().apply {
            putParcelable("Person", person)
        }

        val fragment = MyFragment().apply {
            arguments = fragmentArgs
        }
    }

    override fun onSaveInstanceState(outState: Bundle, outPersistentState: PersistableBundle) {
        super.onSaveInstanceState(outState, outPersistentState)
        outState.putParcelable("Person", person)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        savedInstanceState.getParcelable<Person>("Person")
    }
}