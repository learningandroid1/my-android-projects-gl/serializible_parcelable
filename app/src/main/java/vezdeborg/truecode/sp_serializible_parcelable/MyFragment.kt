package vezdeborg.truecode.sp_serializible_parcelable

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment

class MyFragment: Fragment() {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val args = arguments?.getParcelable<Person>("Person")
    }
}